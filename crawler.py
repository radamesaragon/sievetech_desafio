import requests
import logging
import threading
import _thread
import time
from queue import Queue, Empty
import config
from product import Product


log = logging.getLogger(__name__)
visited_urls = list()


def get_url_host(url):
    starting_http_index = url.find('://') + 3
    ending_slash_index = url.find('/', starting_http_index)
    end_of_hostname = ending_slash_index if ending_slash_index > 0 else len(url)
    return url[:end_of_hostname]


def read_target(target_url):
    try:
        header = requests.head(target_url)

        if 'text/html' not in header.headers['Content-Type']:
            log.debug('Endpoint %s não-HTML', target_url)
            return None

        log.info('Lendo %s', target_url)

        # Delay entre requisições
        time.sleep(int(config.parser['delay_between_requests']))

        response = requests.get(target_url, timeout=60)
        return response.text

    except Exception as err:
        log.error(err)
        return None


def get_page_title(content):
    page_title_tag_index = content.find('<title>')
    page_title_index = content.find('>', page_title_tag_index) + 1
    page_title_closing_tag_index = content.find('</title>', page_title_tag_index)
    return content[page_title_index:page_title_closing_tag_index]


def check_page_is_product(content):
    product_name_div_index = content.find('fn productName')
    product_name_index = content.find('>', product_name_div_index) + 1
    product_name_closing_div_index = content.find('</div>', product_name_div_index)

    if product_name_div_index < 0:
        return None

    return content[product_name_index:product_name_closing_div_index]


def get_inner_links(content, current_url):
    links = list()
    hostname = get_url_host(current_url)
    link_starting = '"' + hostname
    cursor = content.find(link_starting)

    while(cursor >= 0):
        # os +1 consideram e adicionam aspas duplas para delimitar link
        actual_url_index = cursor + 1
        link_ending_index = content.find('"', actual_url_index)
        links.append(content[actual_url_index:link_ending_index])

        cursor = content.find(link_starting, link_ending_index)

    return links


def parse_page(target_url, product_queue):
    if target_url in visited_urls:
        return None

    content = read_target(target_url)
    visited_urls.append(target_url)
    log.debug('URLs visitadas: %d', len(visited_urls))

    if content is not None:
        product_name = check_page_is_product(content)

        if product_name is not None:
            log.info('Produto encontrado: %s', product_name)
            page_title = get_page_title(content)
            product_queue.put(Product(product_name, page_title, target_url))

        return content

    return None


def child_crawler(urls_queue, product_queue, num):
    log.debug('Crawler(%d) iniciando', num)

    keep_looping = True
    while(keep_looping):
        try:
            target_url = urls_queue.get(True, 60)
            parsed_content = parse_page(target_url, product_queue)

            if parsed_content is not None:
                inner_links = get_inner_links(parsed_content, target_url)
                log.debug('Links encontrados: %d', len(inner_links))

                for url in inner_links:
                    urls_queue.put(url)

            urls_queue.task_done()

        except Empty:
            keep_looping = False

        except Exception as err:
            log.error(err)


class WebCrawler (threading.Thread):

    def __init__(self, site_url, product_queue, max_crawlers):
        threading.Thread.__init__(self)
        self.site_url = site_url
        self.product_queue = product_queue
        self.max_crawlers = max_crawlers

    def run(self):
        try:
            urls_queue = Queue()
            urls_queue.put(self.site_url)

            for i in range(self.max_crawlers):
                _thread.start_new_thread(child_crawler, (urls_queue, self.product_queue, i))

            urls_queue.join()

        except Exception as err:
            log.error(err)
