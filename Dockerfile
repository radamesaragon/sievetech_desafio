FROM python:3

WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

RUN python -m unittest

ENV LOG_LEVEL INFO
ENV PARSE_TARGET_URL https://www.epocacosmeticos.com.br
ENV MAX_CRAWLERS 5
ENV REQUESTS_DELAY 2

ENTRYPOINT "python" "./app.py"
