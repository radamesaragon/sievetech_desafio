# Desafio Python para Sievetech

Este módulo examina um site de webcommerce em busca de Produtos e Preços e registra em um CSV.

Briefing: https://bitbucket.org/sievetech/desafio-captura

## Configurações

O programa utiliza as seguintes variáveis de ambiente para configuração:
* LOG_LEVEL: nível do log, havendo as opções básicas do módulo logging ('DEBUG', 'INFO', 'WARNING', 'ERROR')
* PARSE_TARGET_URL: a URL alvo desejada para exame de Produtos e Preços
* MAX_CRAWLERS: número máximo de processadores em paralelo de páginas da URL desejada
* REQUESTS_DELAY: tempo (em segundos) entre as requisições de páginas pelos processadores

### Requisitos

* Docker
* Python3 (opcional para testes externos)

## Instruções para execução

1. Execute `$ docker build -t sievetech_desafio .`
  * Build executa testes
1. Execute `$ docker run -v {~path}/output:/app/output sievetech_desafio`

## Instruções para teste

* Execute `$ python3 -m unittest`

## Resultado

O arquivo CSV que conterá as linhas (configuradas em "Produto", "Título da página" e "URL") será disponibilizado na pasta `/output` no path correspondente à execução do programa.
