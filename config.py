import os

parser = dict(
    log_level = os.getenv('LOG_LEVEL') or 'WARNING',
    target_url = os.getenv('PARSE_TARGET_URL'),
    max_crawlers = os.getenv('MAX_CRAWLERS') or '1',
    delay_between_requests = os.getenv('REQUESTS_DELAY') or '0'
)
