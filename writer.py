import logging
import threading
from queue import Empty


log = logging.getLogger(__name__)
written_products = list()


def write_product(file_descriptor, product):
    if product.get_name() in written_products:
        return None

    log.debug('Escrevendo %s', product.get_name())
    file_descriptor.write('"%s","%s","%s"\n' %
                          (product.get_name(),
                           product.get_page_title(),
                           product.get_url()))
    written_products.append(product.get_name())


class CsvWriter (threading.Thread):

    def __init__(self, site_url, product_queue):
        threading.Thread.__init__(self)
        self.site_url = site_url
        self.product_queue = product_queue

    def run(self):
        filename = self.site_url.replace(
            '//', '_').replace('/', '_').replace('.', '_').replace(':', '')

        try:
            with open('output/%s.csv' % (filename), 'w', 1) as f:
                # Adiciona header do CSV
                f.write('"Produto","Titulo","URL"\n')

                keep_looping = True
                while(keep_looping):
                    try:
                        product = self.product_queue.get(True, 60)
                        write_product(f, product)
                        self.product_queue.task_done()

                    except Empty:
                        keep_looping = False

                    except Exception as err:
                        log.error('(while) %s', err)

        except Exception as err:
            log.error(err)
