import unittest
from unittest.mock import Mock
from queue import Empty
import sys
from product import Product

requests_mock = Mock()
head_mock = Mock()
get_mock = Mock()
requests_mock.attach_mock(head_mock, 'head')
requests_mock.attach_mock(get_mock, 'get')

sys.modules['requests'] = requests_mock
import crawler

class MockResponse:
    pass


class TestCrawler(unittest.TestCase):

    requests_head_html = {'Content-Type': 'text/html'}
    requests_head_not_html = {'Content-Type': 'another/mime-type'}
    requests_get_success_response = MockResponse()
    requests_get_success_response.text = '<html>'
    default_url = 'www.ok_url.com'

    def setUp(self):
        pass

    def set_requests_success(self):
        head_mock.return_value.headers = self.requests_head_html
        head_mock.side_effect = None
        get_mock.return_value.text = self.requests_get_success_response.text
        get_mock.side_effect = None

    def test_get_url_host_http(self):
        self.assertEqual(crawler.get_url_host(
            'http://www.hostname.com/someendpoint'), 'http://www.hostname.com')

    def test_get_url_host_http_no_slash(self):
        self.assertEqual(crawler.get_url_host(
            'http://www.hostname.com'), 'http://www.hostname.com')

    def test_get_url_host_https(self):
        self.assertEqual(crawler.get_url_host(
            'https://www.hostname2.com/anotherendpoint'), 'https://www.hostname2.com')

    def test_get_url_host_www(self):
        self.assertEqual(crawler.get_url_host(
            'www.hostname3.com/lastendpoint'), 'www.hostname3.com')

    def test_read_target_html_success(self):
        self.set_requests_success()
        self.assertEqual(crawler.read_target(self.default_url),
                         self.requests_get_success_response.text)

    def test_read_target_html_error(self):
        head_mock.return_value.headers = self.requests_head_html
        head_mock.side_effect = None
        get_mock.return_value.text = None
        get_mock.side_effect = Exception('requests_get_error')
        self.assertIsNone(crawler.read_target(self.default_url))

    def test_read_target_not_html_success(self):
        head_mock.return_value.headers = self.requests_head_not_html
        head_mock.side_effect = None
        get_mock.return_value.text = None
        get_mock.side_effect = None
        self.assertIsNone(crawler.read_target(self.default_url))

    def test_read_target_not_html_error(self):
        head_mock.return_value.headers = None
        head_mock.side_effect = Exception('requests_head_error')
        get_mock.return_value.text = None
        get_mock.side_effect = None
        self.assertIsNone(crawler.read_target(self.default_url))

    def test_get_page_title_success(self):
        title = 'Teste'
        success_html = '<title>%s</title>' % title
        self.assertEqual(crawler.get_page_title(success_html), title)

    def test_get_page_title_fail(self):
        titleless_html = '<html></html>'
        self.assertEqual(crawler.get_page_title(titleless_html), '')

    def test_check_page_is_product_success(self):
        product_name = 'ProdutoTeste'
        success_product_html = '<div fn productName>%s</div>' % product_name
        self.assertEqual(crawler.check_page_is_product(
            success_product_html), product_name)

    def test_check_page_is_product_false(self):
        false_product_html = '<div fn not productName></div>'
        self.assertIsNone(crawler.check_page_is_product(false_product_html))

    def test_get_inner_links_success(self):
        hostname = 'http://hostname'
        success_links_list = [hostname + '/link1', hostname + '/link2']
        success_links_html = '<a href="%s"></a><javascript link("%s")>' % (success_links_list[0], success_links_list[1])
        self.assertEqual(crawler.get_inner_links(success_links_html, hostname), success_links_list)

    def test_get_inner_links_empty(self):
        hostname = 'http://hostname'
        empty_links_list = []
        empty_links_html = '</html>'
        self.assertEqual(crawler.get_inner_links(empty_links_html, hostname), empty_links_list)

    def test_parse_page_product_true_first_visit(self):
        self.set_requests_success()
        product_name = 'ProdutoTeste'
        page_title = 'Teste'
        full_success_html = '<title>%s</title><div fn productName>%s</div>' % (page_title, product_name)
        get_mock.return_value.text = full_success_html

        product_queue_mock = Mock()

        self.assertEqual(crawler.parse_page(self.default_url, product_queue_mock), full_success_html)
        assert product_queue_mock.put.call_count == 1

    def test_parse_page_product_true_second_visit(self):
        self.set_requests_success()

        product_queue_mock = Mock()

        self.assertIsNone(crawler.parse_page(self.default_url, product_queue_mock))
        assert product_queue_mock.put.call_count == 0

    def test_parse_page_product_false(self):
        self.set_requests_success()
        page_title = 'Teste'
        full_success_html = '<title>%s</title>' % page_title
        get_mock.return_value.text = full_success_html
        new_url = 'www.new_url.com'

        product_queue_mock = Mock()

        self.assertEqual(crawler.parse_page(new_url, product_queue_mock), full_success_html)
        assert product_queue_mock.put.call_count == 0

    def test_child_parse_success(self):
        self.set_requests_success()

        self.get_counter = 0
        def get_mock_results(*args):
            self.get_counter += 1

            if self.get_counter == 1:
                product_name = 'ProdutoTeste'
                page_title = 'Teste'
                full_success_html = '<title>%s</title><div fn productName>%s</div>' % (page_title, product_name)
                get_mock.return_value.text = full_success_html
                return 'www.new_url.com/product'

            elif self.get_counter == 2:
                page_title = 'Teste'
                full_success_html = '<title>%s</title>' % page_title
                get_mock.return_value.text = full_success_html
                return 'www.new_url.com/notproduct'

            elif self.get_counter == 3:
                raise Empty()

        urls_queue_mock = Mock()
        urls_queue_mock.get = Mock(side_effect=get_mock_results)
        urls_queue_mock.task_done = Mock()
        product_queue_mock = Mock()

        crawler.child_crawler(urls_queue_mock, product_queue_mock, 1)
        assert urls_queue_mock.get.call_count == 3
        assert urls_queue_mock.task_done.call_count == 2
        assert product_queue_mock.put.call_count == 1


if __name__ == '__main__':
    unittest.main()
