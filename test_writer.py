import unittest
from unittest.mock import Mock
from queue import Empty
from product import Product
import writer


class TestWriter(unittest.TestCase):

    def setUp(self):
        pass

    def test_write_product(self):
        product_name = 'ProdutoTeste'
        page_title = 'Titulo'
        url = 'www.url_ok.com'
        product = Product(product_name, page_title, url)

        def write_test(string):
            assert string == '"%s","%s","%s"\n' % (product_name, page_title, url)

        file_descriptor_mock = Mock()
        file_descriptor_mock.write = Mock(side_effect = write_test)

        writer.write_product(file_descriptor_mock, product)
        assert file_descriptor_mock.write.call_count == 1


if __name__ == '__main__':
    unittest.main()
