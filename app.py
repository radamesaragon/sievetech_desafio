from queue import Queue
import logging as log
import config
from crawler import WebCrawler
from writer import CsvWriter


def main():
    log.basicConfig(level=getattr(log, config.parser['log_level']))
    log.info('Site alvo: %s', config.parser['target_url'])

    try:
        products_queue = Queue()

        crawler = WebCrawler(config.parser['target_url'], products_queue, int(config.parser['max_crawlers']))
        crawler.start()

        writer = CsvWriter(config.parser['target_url'], products_queue)
        writer.start()

        crawler.join()
        log.info('Fim do scanning do site')
        product_queue.join()
        log.info('Fim da fila de Produtos a registrar')
        writer.join()
        log.info('Fim do Programa')

    except KeyboardInterrupt:
        log.warning('Interrompendo')

    except Exception as err:
        log.error(err)


if __name__ == '__main__':
    main()
