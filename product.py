class Product ():

    def __init__(self, name, page_title, url):
        self.name = name
        self.page_title = page_title
        self.url = url

    def get_name(self):
        return self.name

    def get_page_title(self):
        return self.page_title

    def get_url(self):
        return self.url
